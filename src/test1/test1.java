package test1;

import javax.swing.JOptionPane;

public class test1 {
public static void main(String[] args){
        
        int tamany = Integer.parseInt(JOptionPane.showInputDialog("Introdueix el tamany de l'array"));
        
        int num_aleatoris[] = new int[tamany];
        
        for(int i=0; i<num_aleatoris.length; i++){
            num_aleatoris[i] = (int)Math.random()*100;
        }
        
        for(int element: num_aleatoris){
            System.out.println(element);
        }
        
    }
}
